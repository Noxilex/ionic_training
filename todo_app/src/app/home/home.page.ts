import { Component, OnInit } from '@angular/core';
import { Task } from '../models/Task';
import { TodoService } from '../todo.service';
import { Router } from '@angular/router';
import { map, catchError } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  // tasks: Task[];
  tasks: Object[] = [];

  constructor(private service: TodoService, private router: Router) {
  }

  ngOnInit() {
    this.tasks = [];
  }

  ionViewWillEnter() {
    console.log(this.tasks);
    // this.tasks = this.service.getAllTask();
    this.service.dbGetTasks().subscribe(data => {
      if (data) {
        this.tasks = Object.entries(data);
      } else {
        this.tasks = [];
      }
    });
  }

  ionViewDidEnter() {
    console.log(this.tasks);
  }

  // updateItem(id) {
  //   this.router.navigate(['update-task', id]);
  // }
}
