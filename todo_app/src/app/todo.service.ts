import { Injectable } from '@angular/core';
import { Task } from './models/Task';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  // tasks: Task[] = [];
  tasks: Object[] = [];
  API = 'https://todoapp-5ea92.firebaseio.com';
  table = 'tasks';

  constructor(private http: HttpClient, private toastController: ToastController) { }

  // addTask(task): void {
  //   this.tasks.push(task);
  // }

  // updateTask(id: number, task: Task) {
  //   console.log(id);
  //   this.tasks[id] = task;
  // }

  // getTask(id: number): Task {
  //   return this.tasks[id];
  // }

  // getAllTask(): Task[] {
  //   return this.tasks;
  // }

  deleteTask(id: number) {
    this.tasks.splice(id, 1);
  }

  // POST
  dbAddTask(task: Task): Observable<Task> {
    return this.http.post<Task>(`${this.API}/${this.table}.json`, task);
  }

  // PUT
  dbModifyTask(id: string, task): Observable<any> {
    return this.http.put<Task>(`${this.API}/${this.table}/${id}.json`, task);
  }

  // DELETE
  dbDeleteTask(id: string): Observable<any> {
    return this.http.delete<Task>(`${this.API}/${this.table}/${id}.json`);
  }

  // GET
  dbGetTask(id: string): Observable<Task> {
    return this.http.get<Task>(`${this.API}/${this.table}/${id}.json`);
  }

  // GET ALL - DONE
  dbGetTasks(): Observable<any> {
    return this.http.get(`${this.API}/${this.table}.json`);
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

}
