import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TodoService } from '../todo.service';
import { Task } from '../models/Task';

@Component({
  selector: 'app-update-task',
  templateUrl: './update-task.page.html',
  styleUrls: ['./update-task.page.scss'],
})

export class UpdateTaskPage implements OnInit {

  task: Task = new Task('', '');
  id: string;

  constructor(private service: TodoService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    console.log(this.id);
    this.service.dbGetTask(this.id).subscribe(
      data => {
        console.log(data);
        if (data) {
          this.task = data;
        } else {
          this.service.presentToast('Data not found');
          this.router.navigate(['/home']);
        }
      },
      error => {
        this.service.presentToast('Data not found');
      }
    );
  }

  updateTask(f) {
    const name = f.form.value.name;
    const description = f.form.value.description;
    const task = new Task(name, description);
    // this.service.updateTask(this.id, new Task(name, description));
    // this.router.navigate(['home']);
    this.service.dbModifyTask(this.id, task).subscribe(data => {
      this.router.navigate(['home']);
    });
  }

  deleteTask() {
    this.service.dbDeleteTask(this.id + '').subscribe(data => {
        this.router.navigate(['home']);
      }
    );
  }
}
