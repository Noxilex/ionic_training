import { Component, OnInit } from '@angular/core';
import { TodoService } from '../todo.service';
import { Task } from '../models/Task';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.page.html',
  styleUrls: ['./add-task.page.scss'],
})
export class AddTaskPage implements OnInit {

  constructor(private service: TodoService, private router: Router) { }

  ngOnInit() {
  }

  addTask(f) {
    const name = f.form.value.name;
    const description = f.form.value.description;
    const task = new Task(name, description);
    // this.service.addTask(task);
    // this.router.navigate(['home']);
    this.service.dbAddTask(task).subscribe(data => {
      this.router.navigate(['home']);
    });
  }
}
