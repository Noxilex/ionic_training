# Ionic_Training

### Code made during the training course for Ionic:

- `ionic start <appname> <apptype (blank,tabs...)>` - New ionic app
- <code>ionic lab</code> - Dev interface that showcases the app running
- <code>ionic generate page <pagename></code> - Creates a new page
- <code>ionic genarate service <servicename></code> - Creates a new service

Router - Sert à envoyer des infos grâce à `router.navigate()`
ActivatedRoute - Sert à récupérer des infos grâce à `route.snapshot.paramMap.get('id')`

## Camera Projet:

- Doc: https://ionicframework.com/docs/native/camera
