import { Component, OnInit } from '@angular/core';
import { AstonService } from '../aston.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-formulaire',
  templateUrl: './formulaire.page.html',
  styleUrls: ['./formulaire.page.scss'],
})
export class FormulairePage implements OnInit {

  constructor(private astonService: AstonService, private router: Router) { }

  ngOnInit() {
  }

  ajouter(f) {
    console.log(f.form.value);
    this.astonService.setPrenom(f.form.value.prenom);
    this.router.navigate(['home']);
  }

}
