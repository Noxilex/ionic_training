import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AstonService {

  prenoms = ['Benjamin', 'Julien', 'Thomas', 'Steven'];

  constructor() { }

  getPrenoms() {
    return this.prenoms;
  }

  setPrenom(prenom: string) {
    this.prenoms.push(prenom);
  }
}
