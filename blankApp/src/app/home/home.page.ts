import { Component, OnInit } from '@angular/core';
import { isComponentView } from '@angular/core/src/view/util';
import { AstonService } from '../aston.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  prenoms = [];

  constructor(private astonService: AstonService) {}

  ngOnInit() {
    this.getPrenoms();
  }

  getPrenoms() {
    this.prenoms = this.astonService.getPrenoms();
  }

}
