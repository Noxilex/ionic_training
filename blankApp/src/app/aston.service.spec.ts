import { TestBed } from '@angular/core/testing';

import { AstonService } from './aston.service';

describe('AstonService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AstonService = TestBed.get(AstonService);
    expect(service).toBeTruthy();
  });
});
